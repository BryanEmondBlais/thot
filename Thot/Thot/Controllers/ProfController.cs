﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Thot.Controllers
{
    public class ProfController : Controller
    {
        ThotDbContext db = new ThotDbContext();
        // GET: Prof
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Prof p)
        {
            Prof user = db.prof.Where(u => u.username == p.username && u.password == p.password).FirstOrDefault<Prof>();
            if (user != null)
            {
                return RedirectToAction("ListCours", user);
            }
            else
            {
                return RedirectToAction("Inscription");
            }
        }

        public ActionResult Inscription()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Inscription(Prof p)
        {
            string un = p.nom.Substring(0, 4) + p.prenom.Substring(0, 4);
            var rand = new Random();
            string pw = un.Substring(2, 4) + rand.Next(1000, 9999);
            db.prof.Add(new Prof { nom = p.nom, prenom = p.prenom, email = p.email, username = un, password = pw});
            db.SaveChanges();
            //if (email(p, un, pw))
            //{
            //    ViewBag.mailStatus = "Message sent";
            //    return RedirectToAction("Index");
            //}
            //else
            //{
            //    ViewBag.mailStatus = "Message not sent";
            //    return View();
            //}
            return RedirectToAction("Index");

        }
        public bool email(Student value, string username, string pwuser)
        {
            try
            {
                string subject = "UserName et password pour la prochaine connection";
                string message = "Bonjour voici le username : " + username + " et voici le password : " + pwuser;
                string MailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();
                string pw = System.Configuration.ConfigurationManager.AppSettings["MailPw"].ToString();

                SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 100000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(MailSender, pw);

                MailMessage mailMessage = new MailMessage(MailSender, value.email, subject, message);

                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public ActionResult ListCours()
        {
            return View();
        }
    }
}