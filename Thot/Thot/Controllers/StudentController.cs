﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Thot.Models;

namespace Thot.Controllers
{
    public class StudentController : Controller
    {
        private ThotDbContext db = new ThotDbContext();
        // GET: Student
        public ActionResult Index(Student user)
        {
            if (user.IdStud > 0)
            {
                List<Cours> cours = db.Cours.Where(c => c.IdNiv == user.IdNiv).ToList();
                return View(cours);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        [HttpPost]
        public ActionResult Index(Cours cours)
        {
            return RedirectToAction("cours", cours);
        }

        public ActionResult cours(Cours cours)
        {
            NivScolaire ns = db.niv.Where(n => n.IdNiv == cours.IdNiv).FirstOrDefault<NivScolaire>();
            Prof prof = (Prof)db.prof.Where(p => p.IdProf == cours.IdProf).FirstOrDefault<Prof>();
            Pdf pdf = (Pdf)db.pdf.Where(p => p.IdPdf == cours.IdPdf).FirstOrDefault<Pdf>();
            Solutionnaire sol = (Solutionnaire)db.solutionnaire.Where(s => s.IdSol == cours.sol).FirstOrDefault<Solutionnaire>();
            Quiz quiz = (Quiz)db.quiz.Where(q => q.IdQuiz == cours.quiz).FirstOrDefault<Quiz>();
            //niveauscolair ns = (from n in db.niveauscolairs where n.idNiv == cours.idNiv select n).FirstOrDefault();
            //prof prof = (from p in db.profs where p.Idprof == cours.idProf select p).FirstOrDefault();
            //pdf pdf = (from p in db.pdfs where p.Idpdf == cours.idpdf select p).FirstOrDefault();
            //solutionnaire sol = (from s in db.solutionnaires where s.Idsol == cours.solutionnaires select s).FirstOrDefault();
            //quiz quiz = (from q in db.quizs where q.Idquiz == cours.Quiz select q).FirstOrDefault();


            CoursInfo ci = new CoursInfo(cours.nomDuCours, ns.NivScol, "", "", cours.video, "", "");
            //coursinfo ci = new coursinfo(cours.nomCours,ns.NivScolaire, prof.prenom + " " + prof.nom,pdf.nompfd,cours.video,sol.nomsolutionnaire,quiz.nomquiz);
            //return View(ci);
            return View(ci);
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult login(Student value)
        {

            //Student user = (from u in db.Students where u.username == value.username && u.password == value.password select u).SingleOrDefault<Student>();
            Student user = db.student.Where(u => u.userName == value.userName && u.password == value.password).FirstOrDefault<Student>();
            if (user != null)
            {
                if (user.firstTime == true)
                {
                    return RedirectToAction("changeInfo", user);
                }
                else
                {
                    return RedirectToAction("Index", user);
                }
            }
            else
            {
                return RedirectToAction("Inscription");
            }
        }

        public List<NivScolaire> nivScolair()
        {
            return db.niv.ToList();
        }
        public ActionResult Inscription()
        {
            List<NivScolaire> ls = new List<NivScolaire>();
            ls = nivScolair();
            ViewBag.List = ls;
            return View();
        }
        [HttpPost]
        public ActionResult Inscription(FormCollection objfrm, Student value)
        {
            List<Student> l = new List<Student>();
            string un = value.nom.Substring(0, 4) + value.prenom.Substring(0, 4);
            var rand = new Random();
            string pw = un.Substring(2, 4) + value.email.Substring(0, 5) + rand.Next(1000, 9999);
            db.student.Add(new Student { nom = value.nom, prenom = value.prenom, email = value.email, userName = un, password = pw, IdNiv = value.IdNiv, firstTime = true });
            db.SaveChanges();
            //if (email(value, un, pw))
            //{
            //    ViewBag.mailStatus = "Message sent";
            //    return RedirectToAction("Login");
            //}
            //else
            //{
            //    ViewBag.mailStatus = "Message not sent";
            //    return View();
            //
            return RedirectToAction("Login");
        }
        public bool email(Student value, string username, string pwuser)
        {
            try
            {
                string subject = "UserName et password pour la prochaine connection";
                string message = "Bonjour voici le username : " + username + " et voici le password : " + pwuser;
                string MailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();
                string pw = System.Configuration.ConfigurationManager.AppSettings["MailPw"].ToString();

                SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 100000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(MailSender, pw);

                MailMessage mailMessage = new MailMessage(MailSender, value.email, subject, message);

                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public ActionResult ChangeInfo(Student s)
        {
            return View(s);
        }

        [HttpPost]
        public ActionResult changeInfo(Student user)
        {
            Student s1 = db.student.Where(s => s.nom == user.nom && s.email == user.email).FirstOrDefault<Student>();
            //Student st = (from u in db.Student where u.nom == user.nom && u.email == user.email select u).SingleOrDefault<Student>();

            if (s1 != null)
            {
                s1.userName = user.userName;
                s1.password = user.password;
                s1.firstTime = false;
                db.SaveChanges();
                return RedirectToAction("Index", s1);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
    }
}