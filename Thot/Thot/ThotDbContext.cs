namespace Thot
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class ThotDbContext : DbContext
    {
        // Your context has been configured to use a 'ThotDbContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Thot.ThotDbContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ThotDbContext' 
        // connection string in the application configuration file.
        public ThotDbContext()
            : base("name=ThotDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Thot.ThotDbContext, Thot.Migrations.Configuration>());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public DbSet<Cours> Cours { get; set; }
        public DbSet<NivScolaire> niv { get; set; }
        public DbSet<Student> student { get; set; }
        public DbSet<Quiz> quiz { get; set; }
        public DbSet<Solutionnaire> solutionnaire { get; set; }
        public DbSet<Pdf> pdf { get; set; }
        public DbSet<Prof> prof { get; set; }

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
    public class Cours
    {
        [Key]
        public int IdCours { get; set; }
        public string nomDuCours { get; set; }
        public int IdProf { get; set; }
        public int IdPdf { get; set; }
        public int IdNiv { get; set; }
        public string video { get; set; }
        public int sol { get; set; }
        public int quiz { get; set; }
    }
    public class NivScolaire
    {
        [Key]
        public int IdNiv { get; set; }
        public string NivScol { get; set; }
    }
    public class Pdf
    {
        [Key]
        public int IdPdf { get; set; }
        public string nomPdf { get; set; }
    }
    public class Prof
    {
        [Key]
        public int IdProf { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
    public class Quiz
    {
        [Key]
        public int IdQuiz { get; set; }
        public string nomQuiz { get; set; }
    }
    public class Solutionnaire
    {
        [Key]
        public int IdSol { get; set; }
        public string nomSolutionnaire { get; set; }
    }
    public class Student
    {
        [Key]
        public int IdStud { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string email { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public bool firstTime { get; set; }
        public int IdNiv { get; set; }
    }
}