﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Thot.Models
{
    public class CoursInfo
    {
        public string courNom { get; set; }
        public string nivScol { get; set; }
        public string prof { get; set; }
        public string pdf { get; set; }
        public string video { get; set; }
        public string solutionnaire { get; set; }
        public string quiz { get; set; }

        public CoursInfo() { }
        public CoursInfo(string courNom, string nivScol, string prof, string pdf, string video, string solutionnaire, string quiz)
        {
            this.courNom = courNom;
            this.nivScol = nivScol;
            this.prof = prof;
            this.pdf = pdf;
            this.video = video;
            this.solutionnaire = solutionnaire;
            this.quiz = quiz;
        }
    }
}